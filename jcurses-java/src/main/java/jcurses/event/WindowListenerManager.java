package jcurses.event;

/**
*  This class implements a listener manager to manage {@link jcurses.event.ValueChangedEvent} instances and listener
* on these. Only possible type of handled events is {@link jcurses.event.ValueChangedEvent},
* of managed listeners id {@link jcurses.event.ValueChangedListener}
*/
public class WindowListenerManager extends ListenerManager {
	
	protected void doHandleEvent(Event event, Object listener) {
		((WindowListener)listener).windowChanged((WindowEvent)event);
	}
	
	protected void verifyListener(Object listener) {
		if (!(listener instanceof WindowListener)) {
			throw new RuntimeException("illegal listener type");
		}
	}
	
	protected void verifyEvent(Event event) {
		if (!(event instanceof WindowEvent)) {
			throw new RuntimeException("illegal event type");
		}
	}

}
