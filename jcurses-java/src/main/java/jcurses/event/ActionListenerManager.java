package jcurses.event;


/**
*  This class implements a listener manager to manage {@link jcurses.event.ActionEvent} instances and listener
* on these. Only possible type of handled events is {@link jcurses.event.ActionEvent},
* of managed listeners id {@link jcurses.event.ActionListener}
*/
public class ActionListenerManager extends ListenerManager {
	
	protected void doHandleEvent(Event event, Object listener) {
		((ActionListener)listener).actionPerformed((ActionEvent)event);
	}
	
	protected void verifyListener(Object listener) {
		if (!(listener instanceof ActionListener)) {
			throw new RuntimeException("illegal listener type");
		}
	}
	
	protected void verifyEvent(Event event) {
		if (!(event instanceof ActionEvent)) {
			throw new RuntimeException("illegal event type");
		}
	}

}
