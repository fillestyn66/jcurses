package jcurses.event;

/**
*  The interface has to be impelemented to listen on instances of {@link WindowEvent}
*/
public interface WindowListener {
    /**
    *  The method will be called by an widget, generating {@link WindowEvent} instances,
    * if the listener has been registered by it.
    * 
    * @param event the event occured
    */    
	public abstract void windowChanged(WindowEvent event);

}
