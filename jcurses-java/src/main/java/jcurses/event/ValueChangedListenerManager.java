package jcurses.event;

/**
*  This class implements a listener manager to manage {@link jcurses.event.ValueChangedEvent} instances and listener
* on these. Only possible type of handled events is {@link jcurses.event.ValueChangedEvent},
* of managed listeners id {@link jcurses.event.ValueChangedListener}
*/
public class ValueChangedListenerManager extends ListenerManager {
	
	protected void doHandleEvent(Event event, Object listener) {
		((ValueChangedListener)listener).valueChanged((ValueChangedEvent)event);
	}
	
	protected void verifyListener(Object listener) {
		if (!(listener instanceof ValueChangedListener)) {
			throw new RuntimeException("illegal listener type");
		}
	}
	
	protected void verifyEvent(Event event) {
		if (!(event instanceof ValueChangedEvent)) {
			throw new RuntimeException("illegal event type");
		}
	}

}
