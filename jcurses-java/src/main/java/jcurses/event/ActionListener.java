package jcurses.event;

/**
*  The interface has to be implemented to listen on instances of {@link ActionEvent}
*/
public interface ActionListener {
	
    /**
    *  The method will be called by an widget, generating {@link ActionEvent} instances,
    * if the listener has been registered by it.
    * 
    * @param event the event occured
    */
	public abstract void actionPerformed(ActionEvent event);

}
