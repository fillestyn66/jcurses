package jcurses.event;

/**
*  This class implements a listener manager to manage {@link jcurses.event.ItemEvent} instances and listener
* on these. Only possible type of handled events is {@link jcurses.event.ItemEvent},
* of managed listeners id {@link jcurses.event.ItemListener}
*/
public class ItemListenerManager extends ListenerManager {
	
	protected void doHandleEvent(Event event, Object listener) {
		((ItemListener)listener).stateChanged((ItemEvent)event);
	}
	
	protected void verifyListener(Object listener) {
		if (!(listener instanceof ItemListener)) {
			throw new RuntimeException("illegal listener type");
		}
	}
	
	protected void verifyEvent(Event event) {
		if (!(event instanceof ItemEvent)) {
			throw new RuntimeException("illegal event type");
		}
	}

}
